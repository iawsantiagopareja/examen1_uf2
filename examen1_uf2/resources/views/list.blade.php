@extends('layouts.main')
    
@section('content')
    <div class="container mt-3">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">producto</th>
                <th scope="col">precio</th>
                <th scope="col">descripcion</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td>manzana</td>
                <td>10</td>
                <td>comida</td>
              </tr>
              <tr>
                <th scope="row">2</th>
                <td>manzana</td>
                <td>10</td>
                <td>comida</td>
              </tr>
              <tr>
                <th scope="row">3</th>
                <td>manzana</td>
                <td>10</td>
                <td>comida</td>
              </tr>
              <tr>
                <th scope="row">4</th>
                <td>manzana</td>
                <td>10</td>
                <td>comida</td>
              </tr>
              <tr>
                <th scope="row">5</th>
                <td>manzana</td>
                <td>10</td>
                <td>comida</td>
              </tr>
              <th scope="row">6</th>
              <td>manzana</td>
              <td>10</td>
              <td>comida</td>
            </tr>
            <tr>
              <th scope="row">7</th>
              <td>manzana</td>
              <td>10</td>
              <td>comida</td>
            </tr>
            <tr>
              <th scope="row">8</th>
              <td>manzana</td>
              <td>10</td>
              <td>comida</td>
            </tr>
            <tr>
              <th scope="row">9</th>
              <td>manzana</td>
              <td>10</td>
              <td>comida</td>
            </tr>
            </tbody>
          </table>
    </div>
@endsection